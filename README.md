# Empty Upstream PHP 8.1

This is an update of our previous Pantheon Empty Upstream https://github.com/ahebrank/empty which adds a settings file to set the PHP version to 8.1 for clean installs of Drupal 10. This setting can be overridden at the project level with the includsion of a pantheon.yml.